	DISCUSS AND DEFINE THE PROBLEM.
Do you know what is the most damaging problems in our society?
	it not poverty, terrorism, ignorance
	its DRUG ADDICTION

What are drugs?
	A drug is any substance that alters normal bodily function, resulting any
psychological or behavioral change.

What is drug addiction?
	Drug addiction and drug abuse, choric or habitual use of any chemical substance to alter states of body or mind for other than medically warranted purposes.

Why drug addiction is a problem?
	There are three main problems of drug addiction:
	Health Problems
	Effects On the Brain
	Behavioral Problems

Health Problems:

-Weakens the immune system, [increasing susceptibility to infections.]
-Cause cardiovascular conditions [ranging from abnormal heart rate to heart attacks. Injected drugs can also lead to collapsed veins and infections of the blood vessels and heart valves.]
-Cause nausea, vomiting and abdominal pain.
-Liver Failure [cause the liver to have to work harder]
-Causes seizures, stroke and widespread brain damage [that can impact all aspects of daily life by causing problems with memory, attention and decision-making, icluding sustained mental confusion and permanent brain damage.]

Effects On The Brain

Although initial drug use may be coluntary, drugs have been shown to alter brain chemistry, thich interferes with an individual's ability to make decisions and can lead to compulsive craving, seeking and use. This then becomes a substance dependency.

-All drugs of abuse - nicotine, cocaine, marijuana, and others - effect the brain's "reward" circuit, which is part of the limbic system.

-Drugs hijack this "reward" system, causing unusually large amounts of dopamine to flood the system.

-This flood of dopamine is what causes the "high" or euphoria associated with drug abuse. 

Behavioral Problems


-    Paranoia
-    Aggressiveness
-    Hallucinations
-    Addiction
-    Impaired Judgment
-    Impulsiveness
-    Loss of Self-Control


	
